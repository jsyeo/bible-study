# Titus 1

Titus 1:5-16

main point: godly elders can rebuke false teachers.
purpose: appoint godly elders.

# Quick Observation

Q: How would you break up this passage?
Q: How would you link them together? How does that affect the way we understand v5-9?
Follow up:
  - What happens if we remove v10-16?

v5-9 and v10-16
Paul links them together using the word "For".
The reason for doing v5-9 is in v10-16.
We can't read v5-9 in isolation, we need to read them as a whole.
Therefore I will be going in circles a bit to help us to understand Paul's commands.

If v10-16 is removed, we wouldn't know why he wants titus to appoint elders.

# What is Paul saying in v5-9? 

Q: What areas of an elder's life are being mentioned here?
Follow up:
  - How would you categorise some of these attributes?

Marriage/family, character, and ministry.

Q: What seems to be the focus here?
Follow up:
  - What phrase is being repeated?

His character, his personal godliness.

Q: How would you summarise v5-9?

Appoint godly elders

# What is Paul saying in v10-16?
Here we will see why titus needs elders.

Q: Who seems to be the focus here?
False teachers in city of crete.

Q: What is so bad about them?
Follow up:
  - What harm were they causing?
  - What is the content of their teaching?
  - Were they saints? are they consistent with their teaching?
deceiving people in the church
teaching circumcision as a requirement for salvation
teaching for their own gain, probably for money or fame
disobedient, liars, gluttons, unfit for good work.
Empty talkers, can't back up their words with their lives

Q: If you have a wanted poster for them, what would you write on it? What headline would you use?
Follow up:
  - If you want people to identify them, what would you say about them?

Watch out for empty talkers!

Deceiving you about entering God's kingdom with circumcision but their lives don't look like God's people.

# What is Paul saying as a whole?

Let's put everything together.
Q: So why must titus appoint godly elders?
Follow up:
  - How would they combat the false teachers?
  - Apart from teaching the right thing and rebuking false hood, why must they be above reproach?
  - Why the emphasis on character?
  - Remember what is the overall theme of the book? What was repeated throughout the letter?

The elders can rebuke the false teachers.
They can teach the right thing and combat falsehood.
More than that, their lives are consistent with their teachings. Unlike the
false teachers, they are living examples of what they teach. This consistency
is something the church cannot see in the false teachers.

What is the main point here, what is Paul's big idea?
main point: godly elders can combat false teachers and their teachings.

What does Paul want Titus to do?
purpose: appoint godly elders.

Application:
1. How would you feel if your pastor or bible study leader is the negative version of what's described in v5-9?
Question and doubt the teaching. doubt the power of the gospel.
Might not do what he says.
Hard to live out his teachings if he is not doing it himself.
2. How might we forget or fail to live out this teaching?
Follow up:
  - How might a church like crossing forget or fail to live out this?
  - How might a loving or forgiving church fail to live this out?

e.g. in a church that emphasises strong Bible teaching and handling we might forget the need of godliness in a church leader.
e.g. forgive church leaders if they sin and still let them teach.

potential questions from the ground:
- v15 what does that mean?
It's probably saying if your teaching is bad, your life would be bad too.
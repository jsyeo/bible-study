# Titus 1

Titus 2:1-15

1 But as for you, teach what accords with sound doctrine.  2 Older men are
to be sober-minded, dignified, self-controlled, sound in faith, in love,
and in steadfastness.  3 Older women likewise are to be reverent in
behavior, not slanderers or slaves to much wine. They are to teach what is
good, 4 and so train the young women to love their husbands and children,
5 to be self-controlled, pure, working at home, kind, and submissive to
their own husbands, that the word of God may not be reviled.  6 Likewise,
urge the younger men to be self-controlled.  7 Show yourself in all
respects to be a model of good works, and in your teaching show integrity,
dignity, 8 and sound speech that cannot be condemned, so that an opponent
may be put to shame, having nothing evil to say about us.  9 Slaves are to
be submissive to their own masters in everything; they are to be
well-pleasing, not argumentative, 10 not pilfering, but showing all good
faith, so that in everything they may adorn the doctrine of God our
Savior. 


11 For the grace of God has appeared, bringing salvation for all people,
12 training us to renounce ungodliness and worldly passions, and to live
self-controlled, upright, and godly lives in the present age, 13 waiting
for our blessed hope, the appearing of the glory of our great God and
Savior Jesus Christ, 14 who gave himself for us to redeem us from all
lawlessness and to purify for himself a people for his own possession who
are zealous for good works. 


15 Declare these things; exhort and rebuke with all authority. Let no one
disregard you. 

repeated words or ideas:
- teach and declare v1,3,4,7, 15
- self-controlled v2, 5, 6, 12
- that or "so that" v5, 8, 10
- sound v1,2,8
- train v4, 12
- submissive v5, 9

what is happening in the church?
what is the church like?
how is paul asking Titus to maintain order?

what is this teaching that titus is to teach?

main point: The gospel trains and demands a godly life.
purpose: Live and teach the gospel rightly

# Quick Observation

lots of instructions for older men and women. why?
*probably to maintain or set a certain order or structure*
older men are to be dignified <- worthy of respect
older women are to be reverent <- show respect

older men
^
|
older women

husbands
^
|
younger women


similar instructions for women and slaves: submission

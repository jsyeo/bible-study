# Overview

Introduce yourself, go one around to tell us where you're studying, what you're
studying, and what you miss about Australia now that you are back in Singapore.

Read whole book of Titus together.

I will begin with an overview of Titus. In the overview we will see what Paul seems to be repeating and we will look at why he is writing this letter to titus. This overview will be like guard rails to understanding smaller section of the text.

The mp of each smaller section shouldn't deviate too much from the overall purpose and point of the letter.

## Opening Question
My opening questions are meant to be open ended, so just feel free to answer.

## Q: How can a Christian grow in his or her godliness?

(Not expecting much)
## Did anyone of you spot some repeated themes or words in the book?

It's okay if we don't have but we will discover the overall theme by looking at
the overview and the first study.

## Q: Who was Paul writing to and what do we learn about them?
Follow up:
- What is Titus supposed to do in Crete?
- Is the letter just written to Titus?

split into two halves read Titus 1:4-5 and Titus 3:15 in groups

- written to titus to take care of the church in Crete

Me: In modern day terms, Titus was supposed to pastor the church.
Paul goes around preaching in his missonary journey and then he will assign
pastors to stay there to plant the church and that's the job of Titus.

- written to the wider church

Me: it is not clear who are the direct readers, might be the church in crete,
or to titus' co workers.

Therefore this letter has implications not just to titus but also to the wider
church community. however we need to acknowledge that the primary audience was
titus.

## Q: What was happening in the church that caused Paul to write this letter?
Follow up: What does these two passages say about the church in Crete?

Split up into two groups, read these two passages:
titus 1:10-16
and
Titus 3:9-11
What does these two passages say about the church in Crete?

- insubordinate people
- deceivers
- false teaching about circumcision
- liars, evil, lazy gluttons
- devoting themselves to myths
- deny God by their works
- unfit for good work
- arguments and quarrels
- people were stiring up division
- consciences defiled

## Q: How is Singapore like the city of Crete?
- Immoral Titus 1:12
- Insubordinate people Titus 1:10
- False teachers leading people away Titus 1:11

## Q: What is Titus' task?
Read Titus 2:1-10, Titus 3:1, Titus 3:8, Titus 3:14

Split into two groups, one group read Titus 2:1-10, the other group read the
Titus 3 verses.

Titus has to push the church to live godly lives.

## Q: Let's step back and think for a second. What would happen when Titus is only called to push people to live godly lives?
Follow up:
  - Day in day out, his sermon and bible study is just "live godly lives!"

A:
No motivation. No reason to live godly lives. Probably shallow and external
conformance to godliness.

So the big question is, how is he supposed to help the church to do that?

## In groups, read all three verses and answer what do we learn about godliness in these verses?

Follow up:
  - how are we trained to be godly?
  - In Titus 3:8, what is Titus supposed to say so that the church would devote themselves to good works?

Titus 1:1
truth leads to godliness
OR
truth goes hand in hand with godliness

Titus 2:11-14
God's grace trains us to be godly

Titus 3:3-8
by insisting on the gospel, God's people will then devote themselves to good works.

## Therefore, for a church to be godly, what do they need?

So the key point that paul is saying is this: Preach the gospel so that your
church will live rightly.

## Summary

Let's summarise what we covered so far,

what's happening in the church?
false teaching, ungodly living, divisions and quarrels

what is titus supposed to do?
Push them towards godly living

How?
By preaching the gospel!

## In a sentence, how would you summarise the book?

Complete this sentence:
To fix a messy, immoral church, Titus must ________ so that it leads to ________.

Godly living comes from gospel preaching.
Gospel preaching leads to godly living.

This is super key. I would dare to argue this is how the bible changes lives. The bible changes lives not by slapping us with a list of good works or godly living. Instead the bible changes us by reorienting our belief systems and worldview. It aims ultimately to change what we love by showing us the drama of the gospel. When we are thrilled by who God is and what he has done then we will want to come onboard with what God commands us.

# Study 1
Titus 1:1-4

## Q: What do we learn about Paul's CV?
Follow up:
  - (Don't just parrot what the bible say.) What is Paul actually saying?
  - What does Paul want Titus to know about him?
  - How would you say this in your own words?

Servant of God and an apostle of Jesus
entrusted by the command of God our savior

*Paul is saying that he is legit.*

## Q: What do we learn about God? How does this affect the way we think about Paul's preaching?

God is truthful about his promise of eternal life
Paul is saying something that God already promised ages ago and he is truthful!

*In summary we learn here that Paul and his message is legit because it is God who promised it*

## Q: Why is Paul telling Titus and the wider church this?
Follow up:
  - Why does Paul need to hold up his own authenticity?
  - Why does Paul need to hold up the authenticity of his message?
  - Remember what's the context of the letter?
  - Who was trying to bring the church away from Paul's teaching?

Since the Cretan church was deceived by false teaching from the circumcision party, Paul wrote this to remind him that his preaching is true

## Q: What do we learn about Paul's goals?
Follow up:
  - What do we learn about the relationship between truth and godliness?
  - How would you say this in your own words?

for the elect (God's people) to have
- faith
- knowledge of truth that accords with godliness

Ultimately for them to have eternal life

two ways of understanding this:
- Truth and godliness goes together
- Truth leads to godliness (NIV)

*Paul wants the Christians to know and believe the truth so that they might live rightly and have eternal life.*

## Q: Why does Paul want Titus and the wider church to know this?
Follow up:
  - What is happening in the church?
  - How does knowing this help Titus and the church?
  - If the church is not living rightly, what do they need?

This is Titus' key to helping the church to live rightly. They need to know the truth and the gospel!

(What is something new here apart from the overall theme of the letter?)

What do you think is the big idea?

Why does Paul write this to Titus?

MP: Paul's true preaching leads to godly living
P: Know this!

Application
Q: Why might this truth be surprising?

Easy to focus on the doing instead of the gospel.
We don't think that it is as simple as knowing truth and the gospel.
natural reaction:
If someone is not doing evangelism, we just tell the person to go and evangelise lah!
If someone is not serving, we just tell the person to go and serve lah!
If someone is having a non-christian bf/gf, we just tell the person to just break up.

Q: In light of knowing this, how should we approach our own growth and godly living?

Meditate on the gospel.
Follow up:
  - What are some practical steps we can do to meditate on the gospel?

Q: What are some ways we deny this truth in our own growth and godly living?